# Work Breakdown

The spreadsheet contains propose capability composition work breakdown.

What is important to *FIRST* consider regarding the appropriateness of these items is to figure out how to get as much repetition on these composition units.
- Suggestions
    - to quote projects based on consideration of uplift for each composition unit
    - assign people to these composition units and have those people implement these capabilities in any project
    - to refactors projects which incorporate this into a standard



This relates to the concept of *modular design* which Aurecon is promoting as a [valueable characteristic](https://justimagine.aurecongroup.com/walt-disney-frank-lloyd-wright-chemistry-common/#)


To Read: 
[Article describing up unshared components are the major bottleneck in software development organizations](https://www.industriallogic.com/blog/why-are-component-teams-still-so-popular). This is relevant to shared capabilities discussion.   `from Dion`